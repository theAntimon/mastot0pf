import urequests
import time
import re

from st3m.application import Application, ApplicationContext
from st3m.input import InputState
from st3m.ui.colours import WHITE, BLACK
from ctx import Context
import st3m.run
import time
import _thread
import leds

fetch_interval_seconds = 10
min_seconds_for_each_post = 10
fetch_thread_is_running = False

hashtags = ["cccamp2023", "cccamp23", "flow3r"]

# maps name of instance to id of last received toot. since we start somewhere, that is None at the beginning
instances = [
    "chaos.social",
    "mastodon.social",
    "digitalcourage.social",
    "genserver.social",
    "mellowcodelabs.social",  # mastodon server from Portugal
    "social.aleteoryx.me",  # mastodon server from United States
    "mastodon.gregnilsen.com",  # mastodon server from France
    "essjax.com",  # mastodon server from Portugal
    "ssul.one",  # pleroma server from Singapore
    "ff.pikopublishing.page",  # firefish server from United Kingdom
    "eepy.moe",  # firefish server from United States
    "uni.vuwuv.com",  # akkoma server from Japan
    "eei-misskey.com",  # misskey server from Private
    "7a4.pp.ua",  # pleroma server from Ukraine
    "zlatex.club",  # mastodon server from Portugal
    "pickey.net",  # misskey server from Japan
    "stormwaltz.net",  # mastodon server from Portugal
    "mirtube.ru",  # peertube server from Russia
    "fludiblu.xyz",  # lemmy server from United States
    "fedi.afra.berlin",  # iceshrimp server from Germany
    "lemmy.dannyj.nl",  # lemmy server from Private
    "fedi.crates.io",  # mastodon server from France
    "0qz.fun",  # gotosocial server from Private
    "syfershock.com",  # pleroma server from United States
    "video.expiredpopsicle.com",  # peertube server from United States
    "social.tkschland.de",  # mastodon server from Germany
    "social.busterboys.net",  # akkoma server from United States
    "tube.g4rf.net",  # peertube server from Germany
    "gtube.h0stname.net",  # peertube server from United States
    "social.nbsp.one",  # firefish server from Finland
    "killyour.dad",  # pleroma server from Australia
    "bkcastle.net",  # mastodon server from United States
    "liverpoolfootballclub.social",  # mastodon server from United Kingdom
    "lemmy.demo.coopcloud.tech",  # lemmy server from Germany
    "konpeki.icu",  # dolphin server from Private
    "testsmutlandia.com",  # mastodon server from United States
    "kinryouku.tenshi.zip",  # akkoma server from Canada
    "xenolove.win",  # hometown server from France
    "mastodon.adegeest.net",  # mastodon server from Germany
    "mi.amari-noa.com",  # misskey server from Private
    "mi.halu.ink",  # misskey server from Japan
    "pixel.tiny-host.nl",  # pixelfed server from Germany
    "phar.social",  # mastodon server from United States
    "den6262.net",  # misskey server from Private
    "worldtravel.photos",  # mastodon server from Portugal
    "orc.forest.hostdon.ne.jp",  # mastodon server from Japan
    "mastodon.thx8te.kh.ua",  # mastodon server from Ukraine
    "owncast.rune.party",  # owncast server from United States
    "peertube.tristansuper.com",  # peertube server from France
    "peertube.roundpond.net",  # peertube server from United States
    "social.obco.pro",  # firefish server from Germany
    "social.wagler.it",  # mastodon server from Germany
    "firefish.techguy.social",  # firefish server from United States
    "mespace.social",  # misskey server from Private
    "setariam.net",  # misskey server from Private
    "social.bug.expert",  # lemmy server from Private
    "hd.206267.xyz",  # tootik server from United Kingdom
    "mooneyed.de",  # takahe server from Germany
    "tart.coffee",  # misskey server from Private
    "pixelfed.moe",  # pixelfed server from Japan
    "pixelplanet.social",  # pixelfed server from Netherlands
    "zgz.convoca.la",  # gancio server from Germany
    "lmy.brx.io",  # lemmy server from United Kingdom
    "ooo.eidantoei.org",  # mastodon server from Singapore
    "aquarium.yuri.garden",  # mastodon server from Japan
    "doesstuff.social",  # mastodon server from Private
    "peertube.smertrios.com",  # peertube server from Private
    "social.ztfr.de",  # mastodon server from Germany
    "peertube.estrogen.plus",  # peertube server from Germany
    "lemmy.snekerpimp.space",  # lemmy server from Private
    "ds106.tv",  # peertube server from United States
    "animeomake.com",  # mastodon server from United States
    "social.mchosted.nl",  # mastodon server from Germany
    "icunny.co",  # pleroma server from Germany
    "shotgunlife.social",  # mastodon server from United States
    "sloth.jsnfwlr.ninja",  # gotosocial server from Australia
    "akko.lea.moe",  # akkoma server from Germany
    "brunetti.social",  # mastodon server from United States
    "artsocial.boston",  # mastodon server from United States
    "philiptalk.berwanger-and-kin.com",  # mastodon server from United States
]

queued_posts = []


def unpack_posts(resp) -> [str]:
    def unpack_single_post(content):
        return remove_xml_tags(content)

    posts = []
    try:
        contents = resp.json()
    except ValueError as e:
        print(e)  # apparently it can happen due to some "syntax error in JSON"
        return None
    if len(contents) == 0:
        return None
    for content in contents:
        if (
            content == "error"
        ):  # don't now why this is happening with genserver.social .. .it seems there is some kind of authorization needed
            continue
        user = content["account"]["display_name"]
        content = content["content"]

        post = unpack_single_post(content=content)
        posts.append((user, post))
    return posts


def get_last_posts(*args) -> [str]:
    instance, fetch_num_posts, hashtag = args
    url = "https://" + instance + "/api/v1/timelines/tag/" + hashtag
    url += "?limit=" + str(fetch_num_posts)
    print("url: " + str(url))
    try:
        response = urequests.get(url)

    except Exception as e:
        print(e)
        print("request failed!")
        global fetch_thread_is_running
        fetch_thread_is_running = False
        return None

    ret = unpack_posts(resp=response)
    if ret is not None:
        unpacked_posts = ret
        global queued_posts
        queued_posts += unpacked_posts
    global fetch_thread_is_running
    fetch_thread_is_running = False


def remove_xml_tags(input_string):
    clean_text = re.sub(r"<.*?>", "", input_string)
    return clean_text


def format_string_for_display(input_string, max_line_length=26):
    words = input_string.split()
    lines = []
    current_line = ""

    for word in words:
        if len(current_line) + len(word) <= max_line_length:
            current_line += " " + word if current_line else word
        else:
            lines.append(current_line)
            current_line = word

    if current_line:
        lines.append(current_line)

    formatted_text = "\n".join(lines)
    return formatted_text


class MastoT0pf(Application):
    def __init__(self, app_ctx: ApplicationContext):
        super().__init__(app_ctx)
        self.time_last_fetching = None
        self.intro_text = "warning! this has been hacked together very quick and very dirty in the morning between 3 and 5 am. I will make everything nice after some sleep... \n\n   fetching tuuts now"
        self.current_text = None
        self.time_last_text_popped = None
        self.position_blue_LED = 0
        self.tick = 0
        self.index_current_instance = 0
        self.index_current_hashtag = 0

    def think(self, ins: InputState, delta_ms: int) -> None:
        current_time = time.time()
        global queued_posts

        if len(queued_posts) == 0:
            hashtag = hashtags[self.index_current_hashtag]
            self.index_current_hashtag += 1
            self.index_current_hashtag = self.index_current_hashtag % len(hashtags)
            global fetch_thread_is_running
            if not fetch_thread_is_running:
                if self.time_last_fetching is None:
                    self.fetch_all_posts(hashtag)
                else:
                    time_delta = current_time - self.time_last_fetching
                    if time_delta >= fetch_interval_seconds:
                        self.fetch_all_posts(hashtag)

        if self.intro_text is not None:
            self.current_text = format_string_for_display(self.intro_text)
            self.intro_text = None

        else:
            ret = self.get_content()  # getting current post from queue of posts

            if ret is not None:
                user, content = ret
                print("wrinting content: " + str(content))
                content = format_string_for_display(content)
                self.current_text = "     @" + user + "\n\n" + content

    def fetch_all_posts(self, hashtag):
        # print("fetch_all()")
        self.time_last_fetching = time.time()
        instance = instances[self.index_current_instance]
        self.index_current_instance += 1
        self.index_current_instance = self.index_current_instance % len(instances)
        global fetch_thread_is_running
        fetch_thread_is_running = True
        _thread.start_new_thread(get_last_posts, (instance, 5, hashtag))
        # instance = instances.keys()[0]
        # lastPostId = instances[instance]
        # get_last_posts(instance, lastPostId)

        global queued_posts

    def get_content(self):
        global queued_posts
        if len(queued_posts) == 0:
            return None
        else:
            if self.time_last_text_popped is None:
                self.time_last_text_popped = time.time()
            else:
                time_delta = time.time() - self.time_last_text_popped
                if time_delta >= min_seconds_for_each_post:
                    self.time_last_text_popped = time.time()
                    user, content = queued_posts.pop(0)
                    print("content: " + str(content))
                    return user, content
                else:
                    return None

    def draw(self, ctx: Context) -> None:
        self.tick += 1

        def set_leds(ctx: Context):
            leds.set_all_rgb(0, 0, 0)
            for i in range(0, 40):
                leds.set_rgb(0 + i, 255, 0, 0)
            leds.set_rgb(self.position_blue_LED, 0, 0, 255)
            self.position_blue_LED += 1
            if self.position_blue_LED > 39:
                self.position_blue_LED = 0
            leds.update()

        if self.current_text is not None:
            ctx.rgb(*BLACK).rectangle(-120, -120, 240, 240).fill()

            ctx.font_size = 17
            ctx.font = ctx.get_font_name(0)  # Arimo Regular

            ctx.rgb(1, 1, 1)
            ctx.move_to(-95, -65)  # x, y
            ctx.save()

            ctx.text(self.current_text)
            ctx.restore()
        if self.tick == 10:
            set_leds(ctx=ctx)
            self.tick = 0


if __name__ == "__main__":
    st3m.run.run_view(MastoT0pf(ApplicationContext()))
